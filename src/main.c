/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

/*
 * Dobra, og�em idea jest taka, �eby powielic ten program echuj�cy, co go ju� wcze�niej
 * napisa�em, tylko z u�yciem g�wnobiblioteki. No i po drodze doprowadzic to co�
 * do dzia�ania
 */
#include "main.h"

#define FIFO Robocik_CAN_FilterFIFOAssigment_FIFO0	// fifo kt�rego u�ywamy

void UARTandPinsConfig(void);


int main(void)
{
	UARTandPinsConfig();
	Robocik_CAN_BB(GPIOA->ODR, 5) ^= 1;
	for(int i = 0; i < 1000000; ++i);

	Robocik_CAN_InitTypeDef initTest = {0};
	initTest.CAN_NoAutomaticRetransmition = ENABLE;
	initTest.CAN_LoopbackMode = ENABLE;
	initTest.CAN_ResynchronizationTimeWidth = 1;
	initTest.CAN_TimeSegment2 = 6;
	initTest.CAN_TimeSegment1 = 9;
	initTest.CAN_BaudRatePrescaler = 2;
	Robocik_CAN_Init(&initTest);

	Robocik_CAN_EnableIT_FifoMessagePendingIT(FIFO);

	NVIC_EnableIRQ(USART2_IRQn);
	NVIC_EnableIRQ(USB_LP_CAN1_RX0_IRQn);

	Robocik_CAN_FilterInitTypeDef initFilter = {0};
	// Adres wpisywany w filtr - wykorzystujemy adres krotki (11 bitow),
	// wiec wpisujemy adres w miejsce na adresy skrocone (gorne polslowo, najstarsze 11 bitow)
	// Do gornego polslowa dodano jeszcze 7, w miejsce na adres rozszerzony,
	// zeby zademonstrowac brak wplywu tych bitow na rozpoznanie wiadomosci o skroconym adresie
	initFilter.filterIdHigh = (0x650U << (16U - 11U))|7U;
	initFilter.filterMaskLow = 0x123U;
	initFilter.filterNumber = 0;
	initFilter.filterActivation = ENABLE;
	initFilter.filterMode = Robocik_CAN_FilterMode_Identifier;
	initFilter.filterScale = Robocik_CAN_FilterScale_Single;
	initFilter.filterFIFOAssigment = FIFO;
	Robocik_CAN_FilterInit(&initFilter);

	USART2->DR = 'a';

	for(;;);
}

void UARTandPinsConfig(void)
{
	// tak jak zawsze, podpinamy zegar - pod uart i gpioa
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
	//konfigurujemy pa5 jako wyj�cie, by miec dost�p do kontrolnej di�dki
	GPIOA->CRL = GPIO_CRL_MODE5_1;

	// teraz chwila na konfiguracj� uarta, coby miec kontakt z central�
	// chwila na w*, �e ma�e � mi nie dzia�a xD
	// i do dzie�a
	// na pocz�tek poprawka do konfiguracji pin�w - musimy ustawic je do walki z uartem
	GPIOA->CRL |= (2U << 8U) | (2U << 10U) | (1U << 14U);
	//odrobina zabawy z bitami kontrolnymi
	USART2->CR1 = USART_CR1_RE | USART_CR1_TE | USART_CR1_RXNEIE | USART_CR1_UE;
	// a tutaj ustawiamy szybko�c transmisji na 9600 bod�w
	USART2->BRR = 8000000/9600;
}

void USART2_IRQHandler(void)
{
	Robocik_CAN_BB(GPIOA->ODR, 5) ^= 1;

	volatile char xd = USART2->DR;

	Robocik_CAN_DataFrame data = {0};
	data.stdId = 0x650;
	data.data[0] = ++xd;
	data.DLC = 1U;

	Robocik_CAN_TransmitState err = Robocik_CAN_Transmit(&data);
	if(err != Robocik_CAN_TransmitState_Success)
	{
		for(int x = 0; x < 10; ++x)
		{
			Robocik_CAN_BB(GPIOA->ODR, 5) ^= 1;
			for(int i = 0; i < 1000000; ++i);
		}
	}
}

void USB_LP_CAN_RX0_IRQHandler(void)
{
	Robocik_CAN_DataFrame xd = {0};
	Robocik_CAN_ReceiveState err = Robocik_CAN_Receive(&xd, FIFO);

	USART2->DR = ++(xd.data[0]);
	while(!(USART2->SR & USART_SR_TC));

	if(err != Robocik_CAN_ReceiveState_Success)
	{
		for(int x = 0; x < 10; ++x)
		{
			Robocik_CAN_BB(GPIOA->ODR, 5) ^= 1;
			for(int i = 0; i < 1000000; ++i);
		}
	}
	for(int i = 0; i < 1000; ++i);
}
