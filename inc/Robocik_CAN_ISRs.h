/*
 * Robocik_CAN_ISRs.h
 *
 *  Created on: 04.03.2020
 *      Author: Dell
 */

/*
 * W tym nag��wku b�d� si� znajdowa�y deklaracje i ew. inlajnowe definicje procedur obs�ugi
 * przerwa�. Ma to pozwolic na �atwe odnalezienie ich poprawnych nazw (co mi na przyk�ad
 * w trakcie pisania tej biblioteki troch� czasu zmarnowa�o. Poza tym, maj� si� tu te�
 * znajdowac deklaracje funkcji, kt�re s� domy�lnie wywo�ywane przez przerwania, daj�c
 * dodatkow� warstw� abstrakcji, u�atwiaj�c prac� tym, kt�rzy b�d� korzystac z biblioteki.
 */

#ifndef ROBOCIK_CAN_ISRS_H_
#define ROBOCIK_CAN_ISRS_H_

#include "Robocik_CAN_drajwer.h"

void Robocik_CAN_RX0_Handler(void);
void Robocik_CAN_RX1_Handler(void);

__attribute__((interrupt)) inline void USB_LP_CAN_RX0_IRQHandler(void)
{
//	if((CAN1->RF0R & 3U))		W dalszej cz�ci rozwoju mo�na dodac warunki, kt�re pozwol�
//								wywo�ac odpowiedni� procedur�
		Robocik_CAN_RX0_Handler();
}

__attribute__((interrupt)) inline void CAN_RX1_IRQHandler(void)
{
//	if((CAN1->RF1R & 3U))		W dalszej cz�ci rozwoju mo�na dodac warunki, kt�re pozwol�
//								wywo�ac odpowiedni� procedur�
		Robocik_CAN_RX1_Handler();
}

#endif /* ROBOCIK_CAN_ISRS_H_ */
