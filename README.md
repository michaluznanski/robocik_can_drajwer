# Robocik CAN drajwer
---
Repozytorium zawiera bibliotekę do obsługi kontrolera __bxCAN__ mikrokontrolerów STM32. Zawiera funkcje inicjalizacyjne oraz służace do wysyłania i odbierania wiadomości. Zawiera również struktury i typy, które służą do przekazywania danych do wspomnianych funkcji oraz do uczynienia kodu czytelniejszym.
Oprócz biblioteki, repozytorium zawiera przykładowy program prezentacyjny, którego działanie polega na odbiorze znaków z komputera po UARTcie, przesłaniu ich do samego siebie po CANie w trybie _loopback_, a następnie wysłaniu otrzymanego znaku, po jego uprzedniej inkrementacji, znów poprzez UART do komputera.
